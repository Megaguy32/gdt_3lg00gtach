# 3lg00gtach



A Godot (3.1+) platformer. Enriched by the community.
Focusing on mechanics not usually covered by tutorials.
A learning experience.

**Source code at ** [Gitlab page](https://gitlab.com/Megaguy32/gdt_3lg00gtach/)

**Download at**	[Game Jolt page](https://gamejolt.com/games/3lg00gtach/405225)

![](game_picture_1.png)

- - -

Salutations fellow enthusiasts (◕‿◕)つ

Presented here is a game project being made using the Godot game engine. The Goal is to learn about more complex platformer mechanics that usual tutorials don't cover.
I am Megaguy32 (more personally EthanLR) a South African prospective programmer and Computer Science Student. I hope to come to know fellow game devs through this project; get good help, and pay it forward.

My plan is to log progress as I go and comment my code.
This is my first open source project, so I'll probably feel it out.

This game I've been designing is a platformer.
Focus points are

- Tilemaps & Collision
- Pixel perfect collision
- Slopes
- A cool jump mechanic I've been working on.
- My own interpretation of a state machine

Using open licensed assets, aquired from places like opengameart.org

The ownership of this project is mine, but I suppose you're welcome to contribute, share and use the knowledge gained here.
License and specifics is going to be hazy for now, but I hope we'll get through it unscathed.

Elaborations shall be made in further documents, right now I feel this overview is sufficient.
<br/>

I would greatly appreciate your help. Advice, modifications, suggestions go a long way.