class_name PlayerCharacter
extends KinematicBody2D
"""
Creation date: 2020 Apr 09
Author: EthanLR (Megaguy32)
Description: code to make the main character object work

this code tries to follow PEP 8 coding style, recommended by Godot documentation (3.2)
"""

"""
tags:
	represent the state of the player character, be it running, jumping, attacking, etc.
	Complicated in terms that you may have a combination of tags, and some tags depend on others.
"""

"""
[< Comment date 2020 Jun 12 00h56 >]
	following the beginning of June there's another code restructuring where
	I'm implementing the concurrent state machine logic.
"""

# Section 05. signals:
# - - - end section - - -


# Section 06. enums
# - - - end section - - -


# Section 07. constants
const ATTACK_NODE: PackedScene = preload("res://characters/AttackArea.tscn")
# - - - end section - - -


# Section 08. exported variables

#export(Array, Resource) var tags_array
# - - - end section - - -


# Section 09. public variables



# For enemy fighting queue
		# TODO: Consider making modular, like a "Queue here" extention object
var enemy_fighter_head : Enemy
var enemy_fighter_tail : Enemy


# Tags
var pt := PlayerCharacterTags.new()

# Player movement extention
var class_plc_move = load("res://scripts/player_character/plc_movement.gd")
var plc_move = class_plc_move.new(self) # Cyclic dependency

# - - - end section - - -


# Section 10. private variables
# - - - end section - - -


# Section 11. onready variables

# [< Comment date 2020 Jun 12 02h22 >] prospective code, hopefully make code more modular later on
#onready var plc_movement := $PlayerCharacterMovement
#onready var plc_fighting := $PlayerCharacterFighting
# - - - end section - - -


# Section 12. optional built-in virtual _init method
func _init():
#	print_debug("player character init")
	SingletonPlayerCharacter.player_character = self
	
	for tag in pt.tag_arr:
		tag.connect("tag_value_changed", self, "_tag_value_changed", [ tag ])
	
# - - - end section - - -


# Section 13. built-in virtual _ready method
func _ready():
	pass
	plc_move.determine_input_vector()
# - - - end section - - -


# Section 14. remaining built-in virtual methods

func _process(delta):
	# [< Comment date 2020 Apr 19; 00:15am >] TODO: consider match statment without continue for mutualy exclusive goals
	# [< Comment date 2020 Apr 19; 00:58am >] TODO: consider making tag strings constant variables so I as a coder don't make spelling mistakes, that or modify the tags object to not accept spelling mistakes
	# [< Comment date 2020 Apr 19; 01:16am >] TODO: state / tag system is a lot like package management in linux, consider looking into pre made solutions and also dont forget looking into state machines.
		# take into account - setting true: dependencies, "NOT" dependents
		# take into account - setting false: dependents
	
	
	plc_move.goal_regular_jump()
	plc_move.goal_apply_gravity()
	plc_move.goal_walk()
	plc_move.goal_hunker()
	plc_move.goal_apply_simplified_physics_system()
	plc_move.goal_land()
	plc_move.goal_become_airbourne()
	
	goal_attack()
	
	# read input, set unstable tags
	if Input.is_action_pressed("ui_accept"):
		pt.tags.set_tag(pt.tag_attacking, true)
		
	# goal: set tag moving to false
	# read stable tags, read aux data set unstable tags
	# consider it an orphaned tag
	# set moving to false when no dependers are active and move_velocity is 0
	
	# goal: set tag jumping to false
	# read stable tags, read aux data set unstable tags
	# consider it an orphaned tag
	# Set jumping to false when no dependers are active, and move_velocity is 0
	
	
	# read input, read aux data, set aux data
	plc_move.determine_input_vector()
	
	
	
		
	
	

# - - - end section - - -


# Section 15. public methods

func goal_attack():
	# goal: attack
	# read stable tags, read aux data read environment / visual properties, sets unstable tags TODO: figure out what else this sets, might be something new like "set new instance" or something
	# [< Comment date 2020 Apr 18; 23:10pm >] TODO: Improve this, consider do on attacking initial signal
	if pt.tag_attacking.get_tag_value() == true:
		var attack_node: Node2D = ATTACK_NODE.instance()
		attack_node.position = position + Vector2(32, -24)
		get_tree().current_scene.add_child(attack_node)
		pt.tags.set_tag(pt.tag_attacking, false)


# [< Comment date 2020 Apr 10 >] TODO: consider implemementing input using input map
# Get input from keyboard and put it in vector form







func receiveKnockback(v):
	print("EINA")
	move_and_collide(Vector2(-50,0))


func hurt(hit_value: float, knockback: Vector2):
		print_debug("I, Player Character " + name + " am gehit with a value of: " + str(hit_value))
		pt.tags.set_tag(pt.tag_hurting, true)


func _on_Button_pressed():
	print("ge press")
	pt.tags.set_tag(pt.tag_attacking, true)


# - - - end section - - -


# Section 16. private methods
func _tag_value_changed(tag_value, tag):
	"""here is stuff for when the tags just changed"""
	if tag == pt.tag_airbourning:
		if tag_value == false:
			plc_move.on_land()
# - - - end section - - -
