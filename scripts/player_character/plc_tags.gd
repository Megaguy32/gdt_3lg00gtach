#01. tool

#02. class_name
class_name PlayerCharacterTags

#03. extends
extends Object

#04. # docstring
"""
This code tries to follow the PEP 8 coding style, recommended by Godot documentation
	https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html

Creation date: 2020 Jun 15
Author: EthanLR (Megaguy32)
Description: ***


"""

#05. signals
#06. enums

#07. constants
const tag_airbourning                   = preload ("res://custom_resources/tags_v2020_06_27/tag_airbourning.tres")
const tag_airbourning_phase_ascending   = preload ("res://custom_resources/tags_v2020_06_27/tag_airbourning_phase_ascending.tres")
const tag_airbourning_phase_descending  = preload ("res://custom_resources/tags_v2020_06_27/tag_airbourning_phase_descending.tres")
const tag_airbourning_phase_peaking     = preload ("res://custom_resources/tags_v2020_06_27/tag_airbourning_phase_peaking.tres")
const tag_attacking                     = preload ("res://custom_resources/tags_v2020_06_27/tag_attacking.tres")
const tag_battling                      = preload ("res://custom_resources/tags_v2020_06_27/tag_battling.tres")
const tag_battling_phase_fighting       = preload ("res://custom_resources/tags_v2020_06_27/tag_battling_phase_fighting.tres")
const tag_battling_phase_recovering     = preload ("res://custom_resources/tags_v2020_06_27/tag_battling_phase_recovering.tres")
const tag_controlling                   = preload ("res://custom_resources/tags_v2020_06_27/tag_controlling.tres")
const tag_crouching                     = preload ("res://custom_resources/tags_v2020_06_27/tag_crouching.tres")
const tag_gaurding                      = preload ("res://custom_resources/tags_v2020_06_27/tag_gaurding.tres")
const tag_hunkering                     = preload ("res://custom_resources/tags_v2020_06_27/tag_hunkering.tres")
const tag_hurting                       = preload ("res://custom_resources/tags_v2020_06_27/tag_hurting.tres")
const tag_idling                        = preload ("res://custom_resources/tags_v2020_06_27/tag_idling.tres")
const tag_jumping                       = preload ("res://custom_resources/tags_v2020_06_27/tag_jumping.tres")
const tag_jumping_moon                  = preload ("res://custom_resources/tags_v2020_06_27/tag_jumping_moon.tres")
const tag_jumping_moon_phase_initialing = preload ("res://custom_resources/tags_v2020_06_27/tag_jumping_moon_phase_initialing.tres")
const tag_jumping_moon_phase_resulting  = preload ("res://custom_resources/tags_v2020_06_27/tag_jumping_moon_phase_resulting.tres")
const tag_jumping_regular               = preload ("res://custom_resources/tags_v2020_06_27/tag_jumping_regular.tres")
const tag_moving                        = preload ("res://custom_resources/tags_v2020_06_27/tag_moving.tres")
const tag_normal_posturing              = preload ("res://custom_resources/tags_v2020_06_27/tag_normal_posturing.tres")
const tag_not_asce                      = preload ("res://custom_resources/tags_v2020_06_27/tag_not_asce.tres")
const tag_not_batt_movi                 = preload ("res://custom_resources/tags_v2020_06_27/tag_not_batt_movi.tres")
const tag_not_desc                      = preload ("res://custom_resources/tags_v2020_06_27/tag_not_desc.tres")
const tag_not_figh                      = preload ("res://custom_resources/tags_v2020_06_27/tag_not_figh.tres")
const tag_not_hunk_airb                 = preload ("res://custom_resources/tags_v2020_06_27/tag_not_hunk_airb.tres")
const tag_not_init                      = preload ("res://custom_resources/tags_v2020_06_27/tag_not_init.tres")
const tag_not_moon                      = preload ("res://custom_resources/tags_v2020_06_27/tag_not_moon.tres")
const tag_not_movi                      = preload ("res://custom_resources/tags_v2020_06_27/tag_not_movi.tres")
const tag_not_peak                      = preload ("res://custom_resources/tags_v2020_06_27/tag_not_peak.tres")
const tag_not_reco                      = preload ("res://custom_resources/tags_v2020_06_27/tag_not_reco.tres")
const tag_not_regu                      = preload ("res://custom_resources/tags_v2020_06_27/tag_not_regu.tres")
const tag_not_resu                      = preload ("res://custom_resources/tags_v2020_06_27/tag_not_resu.tres")
const tag_or_asce_peak_desc             = preload ("res://custom_resources/tags_v2020_06_27/tag_or_asce_peak_desc.tres")
const tag_or_figh_reco                  = preload ("res://custom_resources/tags_v2020_06_27/tag_or_figh_reco.tres")
const tag_or_gaur_atta_hurt             = preload ("res://custom_resources/tags_v2020_06_27/tag_or_gaur_atta_hurt.tres")
const tag_or_init_resu                  = preload ("res://custom_resources/tags_v2020_06_27/tag_or_init_resu.tres")
const tag_or_moon_regu                  = preload ("res://custom_resources/tags_v2020_06_27/tag_or_moon_regu.tres")
const tag_sliding                       = preload ("res://custom_resources/tags_v2020_06_27/tag_sliding.tres")
const tag_stancing                      = preload ("res://custom_resources/tags_v2020_06_27/tag_stancing.tres")
const tag_walking                       = preload ("res://custom_resources/tags_v2020_06_27/tag_walking.tres")

const tag_arr = [
	tag_airbourning,
	tag_airbourning_phase_ascending,
	tag_airbourning_phase_descending,
	tag_airbourning_phase_peaking,
	tag_attacking,
	tag_battling,
	tag_battling_phase_fighting,
	tag_battling_phase_recovering,
	tag_controlling,
	tag_crouching,
	tag_gaurding,
	tag_hunkering,
	tag_hurting,
	tag_idling,
	tag_jumping,
	tag_jumping_moon,
	tag_jumping_moon_phase_initialing,
	tag_jumping_moon_phase_resulting,
	tag_jumping_regular,
	tag_moving,
	tag_normal_posturing,
	tag_not_asce,
	tag_not_batt_movi,
	tag_not_desc,
	tag_not_figh,
	tag_not_hunk_airb,
	tag_not_init,
	tag_not_moon,
	tag_not_movi,
	tag_not_peak,
	tag_not_reco,
	tag_not_regu,
	tag_not_resu,
	tag_or_asce_peak_desc,
	tag_or_figh_reco,
	tag_or_gaur_atta_hurt,
	tag_or_init_resu,
	tag_or_moon_regu,
	tag_sliding,
	tag_stancing,
	tag_walking,
]

#08. exported variables
#09. public variables

#var class_tags = load("res://scripts/tag_classes/tags.gd") # Cyclic dependency
var tags: TagsExperimental

#10. private variables
#11. onready variables

#12. optional built-in virtual _init method

func _init():
#	print("plc_tags init")
	clear_dependencies()
	
	tag_airbourning.add_dependee(tag_moving)
	tag_airbourning.add_dependee(tag_or_asce_peak_desc)
	
	tag_airbourning_phase_ascending.add_dependee(tag_airbourning)
	tag_airbourning_phase_ascending.add_dependee(tag_not_peak)
	
	tag_airbourning_phase_descending.add_dependee(tag_airbourning)
	tag_airbourning_phase_descending.add_dependee(tag_not_peak)
	
	tag_airbourning_phase_peaking.add_dependee(tag_airbourning)
	tag_airbourning_phase_peaking.add_dependee(tag_not_asce)
	tag_airbourning_phase_peaking.add_dependee(tag_not_desc)
	
	# tag_attacking # no dependees
	
	tag_battling.add_dependee(tag_or_figh_reco)
	
	tag_battling_phase_fighting.add_dependee(tag_battling)
	tag_battling_phase_fighting.add_dependee(tag_or_gaur_atta_hurt)
	tag_battling_phase_fighting.add_dependee(tag_not_reco)
	
	tag_battling_phase_recovering.add_dependee(tag_battling)
	tag_battling_phase_recovering.add_dependee(tag_not_figh)
	
	# tag_controlling # no dependees
	
	tag_crouching.add_dependee(tag_hunkering)
	
	# tag_gaurding # no dependees
	
	# tag_hunkering
	
	# tag_hurting
	
	tag_idling.add_dependee(tag_not_batt_movi)
	tag_idling.add_dependee(tag_normal_posturing)
	
	tag_jumping.add_dependee(tag_airbourning)
	tag_jumping.add_dependee(tag_or_moon_regu)
	
	tag_jumping_moon.add_dependee(tag_jumping)
	tag_jumping_moon.add_dependee(tag_not_regu)
	tag_jumping_moon.add_dependee(tag_or_init_resu)
	
	tag_jumping_moon_phase_initialing.add_dependee(tag_jumping_moon)
	tag_jumping_moon_phase_initialing.add_dependee(tag_not_resu)
	
	tag_jumping_moon_phase_resulting.add_dependee(tag_jumping_moon)
	tag_jumping_moon_phase_resulting.add_dependee(tag_not_init)
	
	tag_jumping_regular.add_dependee(tag_jumping)
	tag_jumping_regular.add_dependee(tag_not_moon)
	
	# tag_moving
	
	tag_normal_posturing.add_dependee(tag_not_hunk_airb)
	
	
	tag_not_asce.add_dependee(tag_airbourning_phase_ascending)
	
	tag_not_batt_movi.add_dependee(tag_battling)
	tag_not_batt_movi.add_dependee(tag_moving)
	
	tag_not_desc.add_dependee(tag_airbourning_phase_descending)
	
	tag_not_figh.add_dependee(tag_battling_phase_fighting)
	
	tag_not_hunk_airb.add_dependee(tag_hunkering)
	tag_not_hunk_airb.add_dependee(tag_airbourning)
	
	tag_not_init.add_dependee(tag_jumping_moon_phase_initialing)
	
	tag_not_moon.add_dependee(tag_jumping_moon)
	
	tag_not_movi.add_dependee(tag_moving)
	
	tag_not_peak.add_dependee(tag_airbourning_phase_peaking)
	
	tag_not_reco.add_dependee(tag_battling_phase_recovering)
	
	tag_not_regu.add_dependee(tag_jumping_regular)
	
	tag_not_resu.add_dependee(tag_jumping_moon_phase_resulting)
	
	
	tag_or_asce_peak_desc.add_dependee(tag_airbourning_phase_ascending)
	tag_or_asce_peak_desc.add_dependee(tag_airbourning_phase_peaking)
	tag_or_asce_peak_desc.add_dependee(tag_airbourning_phase_descending)
	
	tag_or_figh_reco.add_dependee(tag_battling_phase_fighting)
	tag_or_figh_reco.add_dependee(tag_battling_phase_recovering)
	
	tag_or_gaur_atta_hurt.add_dependee(tag_gaurding)
	tag_or_gaur_atta_hurt.add_dependee(tag_attacking)
	tag_or_gaur_atta_hurt.add_dependee(tag_hurting)
	
	tag_or_init_resu.add_dependee(tag_jumping_moon_phase_initialing)
	tag_or_init_resu.add_dependee(tag_jumping_moon_phase_resulting)
	
	tag_or_moon_regu.add_dependee(tag_jumping_moon)
	tag_or_moon_regu.add_dependee(tag_jumping_regular)
	
	tag_sliding.add_dependee(tag_moving)
	tag_sliding.add_dependee(tag_hunkering)
	
	tag_walking.add_dependee(tag_moving)
	tag_walking.add_dependee(tag_normal_posturing)
	
	
	tags = TagsExperimental.new(tag_arr)


#13. built-in virtual _ready method
#14. remaining built-in virtual methods
#15. public methods

func clear_dependencies():
	for tag in tag_arr:
		tag.dependees = []
		tag.dependers = []

#16. private methods
