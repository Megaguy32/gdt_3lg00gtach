#01. tool

#02. class_name
class_name PlayerCharacterMovement

#03. extends
extends Object

#04. # docstring
"""
This code tries to follow the PEP 8 coding style, recommended by Godot documentation
	https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html

Creation date: 2020 June 12
Author: EthanLR (Megaguy32)
Description: ***


"""

#05. signals
#06. enums
#07. constants
const VEC_GROUND := Vector2(0, 1)

#08. exported variables
export (float) var walk_speed: float = 8
export (float) var jump_speed: float = -8
#export (float) var change_a_remainder: float = 0.5
#export (float) var change_v_initial: float = -30
export (float) var dampen := 1
export (Vector2) var gravity := Vector2(0, 0.5)
# - - - end section - - -

#09. public variables
var plc #: PlayerCharacter # Cyclic dependency

var move_velocity: Vector2 = Vector2(0,0)
var move_collision: KinematicCollision2D = null
var input_vector: Vector2 = Vector2()
# - - - end section - - -

#10. private variables
#11. onready variables

#12. optional built-in virtual _init method
func _init(player_character): # Cyclic dependency
#	print("plc_movement init")
	self.plc = player_character
#13. built-in virtual _ready method

#14. remaining built-in virtual methods

func _process(delta):
	pass
# - - - end section - - -

#15. public methods
func goal_regular_jump():
	# goal: regular jump?
	# read stable tags, read aux data, set unstable tags
	if input_vector.y < 0 and plc.pt.tag_airbourning.get_tag_value() == false:
		
		print("PLC move here, setting jump to true!")
		plc.pt.tags.set_tag(plc.pt.tag_jumping_regular, true)
		
		# set move_velocity?
		move_velocity.y = -18


func goal_apply_gravity():
	# goal: apply gravity 
	# read stable tags, set aux data
	if plc.pt.tag_airbourning.get_tag_value() == true:
		move_velocity += gravity
		
		
func goal_walk():
	# goal: walk
	# read stable tags, read aux data, set aux data
	if input_vector.x != 0:
		move_velocity.x = input_vector.x * walk_speed # [< Comment date 2020 Apr 10 >] TODO: consider move per second
#		print("gonna move")
		if plc.pt.tag_airbourning.get_tag_value() == false:
			plc.pt.tags.set_tag(plc.pt.tag_walking, true)
			
	else:
			move_velocity.x = 0
			
			if plc.pt.tag_walking.get_tag_value() == true:
#				print("### Set walking to false")
				plc.pt.tags.set_tag(plc.pt.tag_walking, false)
			
			
func goal_hunker():
	# goal: hunker
	# read aux data, set unstable tag
	if input_vector.y == 1:
		plc.pt.tags.set_tag(plc.pt.tag_hunkering, true)


func goal_apply_simplified_physics_system():
	# goal: apply simplified physics system
	# read stable tags, read aux data, set visual properties
	move_collision = plc.move_and_collide(move_velocity)
	
	if move_collision != null:
		move_along_slopes(move_collision)
	
	# [< Comment date 2020 Apr 18; 23:01pm >] TODO: make this work even though # repeatedly move along surfaces makes the vector (0, 0). consider introducing accurate and effective value
	move_velocity.x = move_toward(move_velocity.x, 0, dampen)
	

func goal_land():
	# environment check
	# read env, set unstable tags
	# check if player is on ground or in air
	var is_collided: bool = plc.test_move(plc.transform, VEC_GROUND)
	if is_collided:
		# land
		# TODO: consider improving, shouldn't have to set move_velocity here!, consider making land a seperate goal
			# ok this has become more complicated than I anticipated, state change requires the dependents to be set to false, so its like dependencies in reverse. think about this
		if plc.pt.tag_airbourning.get_tag_value() == true:
			plc.pt.tags.set_tag(plc.pt.tag_airbourning, false)
			
func on_land():
	move_velocity.y = 0
		
		
func goal_become_airbourne():
	# environment check
	# read env, set unstable tags
	# check if player is on ground or in air
	var is_collided: bool = plc.test_move(plc.transform, VEC_GROUND)
	if !is_collided:
		# being airbourning implies tag moving, being grounded does not (but player might still be moving)
		if plc.pt.tag_airbourning.get_tag_value() == false:
			plc.pt.tags.set_tag(plc.pt.tag_airbourning, true)


func is_gradual(the_normal): # [< 2020 Apr 18; 22:44pm >] TODO: consider more accurate rename
	#checks if normal of collision is facing upwards (checking if its a gradual slope (true), or steep slope / wall (false))
	var vector_1 = Vector2( 0.9,-1) #0.9 is for a little bit of leniency
	var vector_2 = Vector2(-0.9,-1)
	
	var report = (vector_1.dot(the_normal) >= 0) and (vector_2.dot(the_normal) >= 0)
	return report
	
	
func move_along_slopes(move_collision):
	""" repeatedly move along surfaces """
	# [< comment date 2020 Apr 18; 22:38pm >] TODO: consider checking if the angle / normal change / delta is too strenuous (e.g. too steep) look into improving "check_if_slope" function
	while true:
		var the_normal :Vector2 = move_collision.get_normal()
		var move_velocity_1 = move_collision.remainder.slide(the_normal) # get the new vect that shall move up the assumed slope
		if move_velocity_1 == Vector2(0, 0): # stop if wall is hit (relatively speaking)
			break
		move_collision = plc.move_and_collide(move_velocity_1)
		if move_collision == null: # stop if moved without problem
			break


func determine_input_vector(): 
	input_vector = Vector2(0,0)
	
	if Input.is_action_pressed("ui_right"):
		input_vector.x += 1
	if Input.is_action_pressed("ui_left"):
		input_vector.x -= 1
	if Input.is_action_pressed("ui_up"):
		input_vector.y -= 1
	if Input.is_action_pressed("ui_down"):
		input_vector.y += 1
		
#	input_vector = input_vector.normalized()

#16. private methods




