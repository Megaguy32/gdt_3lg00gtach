extends Node
class_name EnemyFighting
# [< comment date 2020 Apr 01 >] this code is heavily coupled with Enemy

signal GoToPositionSignal(thePosition)

onready var plc : scr_states = variableKeepPlayerCharacter.thePlayerCharacter  #the player character
var enemyAhead : Enemy
var enemyBehind : Enemy
var placeInQueue : Vector2

#var inLineIsHeadFighter : bool
#var inLineIsInLine : bool
#var alert = true
const socialDistance = 32

func _ready():
	print_debug("assert that EnemyFighting is child of Enemy only")
#	assert(parent is Enemy) #Yes this code is coupled, perhaps make it a tag and not a parent check though because it might not just be an enemy
	
	if (plc == null):	return
#	print_debug("I, " + theThug.name + ", am gonna get ya, " + thePlayerCharacter.name + "!")
	
func OnBecomeAlert():
	QueueToAttackPlayerCharacter()

			
# Puts thug in line to attack the PlayerCharacter
#func QueueAttackPlayerCharacter(): - - - depreciated by Megaguy32
##	inLineIsInLine = true
##	inLineIsHeadFighter = false
#	if theThug.position.x > thePlayerCharacter.position.x:
#		if (variableKeepPlayerCharacter.headFighter_Right == null):
#			variableKeepPlayerCharacter.headFighter_Right = theThug
#			inLineIsHeadFighter = true
#		else:
#			var bo = variableKeepPlayerCharacter.headFighter_Right.get_node("EnemyFighting")
#			while (bo.inLineThugBehind != null):
#				bo = bo.inLineThugBehind.get_node("EnemyFighting")
#
#			bo.inLineThugBehind = theThug
#			inLineThugAhead = bo.theThug
#	else:
#		if (variableKeepPlayerCharacter.headFighter_Left == null):
#			variableKeepPlayerCharacter.headFighter_Left = theThug
#			inLineIsHeadFighter = true
#		else:
#			#stuffs missing heere, but ma tired
#
#			variableKeepPlayerCharacter.headFighter_Left.get_node("EnemyFighting").inLineThugBehind = theThug
#			inLineThugAhead = variableKeepPlayerCharacter.headFighter_Left

func QueueToAttackPlayerCharacter():
	if plc.enemyFighterHead == null: # if the line is empty
		placeInQueue = GetInitialQueuePosition()
		emit_signal("GoToPositionSignal", placeInQueue)
		plc.enemyFighterHead = self.get_parent() #please decouple
		plc.enemyFighterTail = self.get_parent() #please decouple
		#send attack signal
		
	else: # go to back of line
		placeInQueue = GetNextInQueuePosition()
		emit_signal("GoToPositionSignal", placeInQueue)
		enemyAhead = plc.enemyFighterTail
		plc.enemyFighterTail.enemyFighting.enemyBehind = self.get_parent() #please improve, please decouple
		plc.enemyFighterTail = self.get_parent() #please decouple
		#send attack signal
	
func GetInitialQueuePosition():
	var thePosition : Vector2 = plc.position
	thePosition.x += socialDistance # haha lockdown :]
	return thePosition
	
func GetNextInQueuePosition():
	var thePosition : Vector2 = plc.enemyFighterTail.enemyFighting.placeInQueue
	thePosition.x += socialDistance # haha lockdown :]
	return thePosition
	

	
#func IdleInAttackLine():
#	GoToXPosition(GetAttackLinePosition())
##	LookCoolInLine()
	
#func GoToXPosition(theX):
#	var deltaX = theX - theThug.position.x
#	print_debug("I, " + theThug.name + ", was at position, " + str(theThug.position) + "!")
#	theThug.move_and_collide(Vector2(deltaX,0))
#	print_debug("I, " + theThug.name + ", am at position, " + str(theThug.position) + "!")
	
#func GetAttackPosition():
#	return thePlayerCharacter.position.x + 32
	
#func GetAttackLinePosition():
#	var bo = variableKeepPlayerCharacter.headFighter_Right.get_node("EnemyFighting")
#	while (bo.inLineThugBehind != null):
#		bo = bo.inLineThugBehind.get_node("EnemyFighting")
#	return bo.theThug.position.x + 32
	
#func Attack():
#	print_debug("I enemy, " + theThug.name + ", attempt to attack the player, " + thePlayerCharacter.name + "!")
	
#func LookCoolInLine():
#	pass

#static func TestStaticFunction():
#	pass
