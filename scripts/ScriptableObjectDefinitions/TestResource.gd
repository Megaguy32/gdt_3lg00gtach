extends Resource
class_name TestResource

export var hoob : String = "am hoob"
export var hooboi : int = 1

export(int, FLAGS, "this means 1", "means 2", "means 4 in binary") var boi : int = 0

export var marray : Array
export(Array, int, "Red", "Green", "Blue") var enums = [2, 1, 0]

export var MegamanX = {}



func sayhoob():
	print(hoob)
	print("bitflag: "+ str(boi))
	
func inchooi():
	hooboi+=1
	print(hooboi)
