class_name TagOperatorOr
extends Tag
"""
Creation date: 2020 June 07
Author: EthanLR (Megaguy32)
Description: tag or, now with handler

this code tries to follow PEP 8 coding style, recommended by Godot documentation (3.2)
"""

# 05. signals
# 06. enums
# 07. constants

# Section 08. exported variables
#export(String) var name

export(
	int,
	"asce_peak_desc",
	"figh_reco",
	"gaur_atta_desc",
	"init_resu",
	"moon_regu"
	) var tag_or_case = 0
#export a flag of or options
# - - - end section - - -


# 09. public variables
var or_options # is the flag
# - - - end section - - -


# 10. private variables
# 11. onready variables
#
# 12. optional built-in virtual _init method
# 13. built-in virtual _ready method
# 14. remaining built-in virtual methods


# 15. public methods
func operate():
	var report = false
	for dep in dependees:
		if dep.get_tag_value() == true:
			report = true
			break
			
	set_tag_value(report)

func or_handle():
	match tag_or_case:
		0:
			pass
		1:
			pass
		2:
			pass

# 16. private methods
