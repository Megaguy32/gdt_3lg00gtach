class_name Tag #hgfdskghkljdfhskjhgkljhsg
extends Resource
"""
Creation date: 2020 Apr 29
Author: EthanLR (Megaguy32)
TODO: add link to gitlab repo, no its shoulddd be in reetme
Description: tired, slow, bored, not getting done. plz work.
	ok ok, getting somewhere. thoughts becoming cohesive (Zen dev mode deactivate a little)
	Remembering Heartbeast's tutorial on scriptable objects.

this code tries to follow PEP 8 coding style, recommended by Godot documentation (3.2)
"""

# 05. signals
signal tag_value_changed(tag_value)
# 06. enums
# 07. constants

# Section 08. exported variables
export(String) var name
export(bool) var _value
export(Array, Resource) var dependees = [] # array of Tag
"""export(Array, Resource)""" var dependers = [] # array of Tag
#var tag_dep_tree_transitive = TagDepTreeTransitive.new()

# - - - end section - - -

# 09. public variables
var dependees_transitive # tags that this tag requires when triggered
var dependers_transitive # tags that this tag impacts when triggered
# - - - end section - - -

# 10. private variables
# 11. onready variables
#
# 12. optional built-in virtual _init method
# 13. built-in virtual _ready method
# 14. remaining built-in virtual methods

func _to_string():
	return (name + ": " + str(_value))
	
# - - - end section - - - 


# 15. public methods

func set_tag_value(tag_value):
	#if values are as they should be
	_value = tag_value
	emit_signal("tag_value_changed", tag_value)


func get_tag_value():
	return _value
	
func operate():
	pass
	
func add_dependee(tag: Tag):
	dependees.append(tag)
	if !tag.dependers.has(self):
		tag.dependers.append(self)
	
# - - - end section - - -

	

# 16. private methods
