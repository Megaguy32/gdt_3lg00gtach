#01. tool

#02. class_name
class_name TagsExperimental

#03. extends
extends Object

#04. # docstring
"""
This code tries to follow the PEP 8 coding style, recommended by Godot documentation
	https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html

Creation date: 2020 June 10
Author: EthanLR (Megaguy32)
Description: Represent tags with a collection of tag resources.
	allows for transitive dependencies, and being printed so it may be displayed.

TODO: merge this with tags_stable.gd
"""

#05. signals
#06. enums
#07. constants
const DEBUG_IS_PRINT := true #print out debbuging information if true
#08. exported variables
#09. public variables
var tags : Array # Array of Tag

#10. private variables
#11. onready variables

#12. optional built-in virtual _init method
func _init(var tags_arr: Array):
#	print("tags init")
	tags = tags_arr

#13. built-in virtual _ready method
#14. remaining built-in virtual methods
func _to_string():
	var return_val = "" #"tags is to stringing, HAZZAH!"
	for tag in tags:
		return_val += "\n" + str(tag)
	return return_val

#15. public methods






func get_tags():
	return tags

# Tag traversal specific values
enum rules {BECOME_TRUE, BECOME_FALSE, NOT_APPLICABLE, MIGHT_TRUE, MIGHT_FALSE}
enum depend {DEPENDEE, DEPENDER}
var queue_tag := [] # queue of tags
var tags_handled := []
var queue_query := []

func set_tag(tag_target: Tag, the_value: bool):
	"""
	View my documentation 'Tag traversal' to get a high level understanding of 
	whats happening here.
	
	when you change a single tag value, it has a ripple effect on its 
	neighbouring tags (dependencies), this code calculates what the dependencies
	become.
	
	There is a queue of tags that are dealt with, and if a tag's value is
	uncertain, then it is dealt with last
	"""
	if DEBUG_IS_PRINT:
		print("\nTags setting tag: " + tag_target.name + ", to: " + str(the_value))
	
	tag_target.set_tag_value(the_value)
	
#	if DEBUG_IS_PRINT:
#		print("Target tag set")
	
	queue_tag = [tag_target]
	tags_handled = [tag_target]
	queue_query = []
	
#	if DEBUG_IS_PRINT:
#		print("Entering the big loop")
	
	var temp1 := 0
	
	while queue_tag.empty() == false:
		print("\tphase 1")
		
		while queue_tag.empty() == false:
#			if DEBUG_IS_PRINT:
#				print("\t\tphase 2")
			
			# queue_tag gets emptied as each element is inspected
			var tag = queue_tag.pop_front()
#			if DEBUG_IS_PRINT:
#				print("\t\tNext Tag in line: " + str(tag))
			
			
			for tag_dep in tag.dependees:
				# skip if already handled
				if tags_handled.has(tag_dep):
					continue
				
				var tag_current_value = tag.get_tag_value()
				var the_tag = tag
				var the_depend = depend.DEPENDEE
				var the_tag_dep = tag_dep

				var rule = apply_tag_traversal_rule(tag_current_value, the_tag, the_depend, the_tag_dep)
				the_rest_of_tag_traversal(rule, tag_dep)
				

			for tag_dep in tag.dependers:
				# skip if already handled
				if tags_handled.has(tag_dep):
					continue
					
				var tag_current_value = tag.get_tag_value()
				var the_tag = tag
				var the_depend = depend.DEPENDER
				var the_tag_dep = tag_dep
				
				var rule = apply_tag_traversal_rule(tag_current_value, the_tag, the_depend, the_tag_dep)
				the_rest_of_tag_traversal(rule, tag_dep)
		
		# it deal with the uncertain values now, add them to the queue
		while queue_query.empty() == false:
			var tag_query = queue_query.pop_front()
			tag_query.operate()
			queue_tag.append(tag_query)
			
			
func apply_tag_traversal_rule(value_tag: bool, the_tag: Tag, the_depend: int, the_tag_dep: Tag) -> int:
	# report is one of these constants (BECOME_TRUE, BECOME_FALSE, NOT_APPLICABLE, MIGHT_TRUE, MIGHT_FALSE)
	# [< Comment date 2020 Jun 27 >] TODO: Consider making a regular Tag class,
		# because all tags count as Tag, yes this an important destinction
	
	# becomes true	T ○ ⟶ ⊛
	if value_tag == true and the_tag is TagRegular and the_depend == depend.DEPENDEE and the_tag_dep is Tag:
		return rules.BECOME_TRUE
		
	# becomes true	T ⊛ ⟵ ⊚
	if value_tag == true and the_tag is Tag and the_depend == depend.DEPENDER and the_tag_dep is TagOperatorOr:
		return rules.BECOME_TRUE
		
	# becomes false	T ⊖ ⟶ ⊛
	if value_tag == true and the_tag is TagOperatorNot and the_depend == depend.DEPENDEE and the_tag_dep is Tag:
		return rules.BECOME_FALSE
	
	# becomes false	F ⊚ ⟶ ⊛
	if value_tag == false and the_tag is TagOperatorOr and the_depend == depend.DEPENDEE and the_tag_dep is Tag:
		return rules.BECOME_FALSE
	
	# becomes false	F ⊛ ⟵ ○
	if value_tag == false and the_tag is Tag and the_depend == depend.DEPENDER and the_tag_dep is TagRegular:
		return rules.BECOME_FALSE
	
	# becomes false	T ⊛ ⟵ ⊖
	if value_tag == true and the_tag is Tag and the_depend == depend.DEPENDER and the_tag_dep is TagOperatorNot:
		return rules.BECOME_FALSE
		
	# Not applicable	F ○ ⟶ ⊛
	if value_tag == false and the_tag is TagRegular and the_depend == depend.DEPENDEE and the_tag_dep is Tag:
		return rules.NOT_APPLICABLE
	
	# Not applicable	T ⊛ ⟵ ○
	if value_tag == true and the_tag is Tag and the_depend == depend.DEPENDER and the_tag_dep is TagRegular:
		return rules.NOT_APPLICABLE
		
	# might true	T ⊚ ⟶ ⊛
	if value_tag == true and the_tag is TagOperatorOr and the_depend == depend.DEPENDEE and the_tag_dep is Tag:
		return rules.MIGHT_TRUE
	
	# might true	F ⊖ ⟶ ⊛
	if value_tag == false and the_tag is TagOperatorNot and the_depend == depend.DEPENDEE and the_tag_dep is Tag:
		return rules.MIGHT_TRUE
	
	# might true	F ⊛ ⟵ ⊖
	if value_tag == false and the_tag is Tag and the_depend == depend.DEPENDER and the_tag_dep is TagOperatorNot:
		return rules.MIGHT_TRUE
		
	# might false	F ⊛ ⟵ ⊚
	if value_tag == false and the_tag is Tag and the_depend == depend.DEPENDER and the_tag_dep is TagOperatorOr:
		return rules.MIGHT_FALSE
	
	# if none of those work then there is a case I havent accounted for
	var print_val = ""
	print_val += "ERROR, unhandled tag traversal case"
	print_val += "\nTag value: " + str(value_tag)
	print_val += "\nTag: " + str(the_tag)
	print_val += "\nDependency type (0-Dependee, 1-Depender): " + str(the_depend)
	print_val += "\nThe dependecy tag: " + str(the_tag_dep)
	print_debug(print_val)
	assert(false) # perposfully crash the game
	return 0 # unnecessary return value to satisfy linting


func the_rest_of_tag_traversal(rule: int, tag_dep: Tag):
	match rule:
		rules.BECOME_TRUE:
			tag_dep.set_tag_value(true)
			tags_handled.append(tag_dep)
			queue_tag.append(tag_dep)
			if DEBUG_IS_PRINT:
				print("\t\tI set tag" + str(tag_dep))
			
		rules.BECOME_FALSE:
			tag_dep.set_tag_value(false)
			tags_handled.append(tag_dep)
			queue_tag.append(tag_dep)
			if DEBUG_IS_PRINT:
				print("\t\tI set tag " + str(tag_dep))
			
			
		rules.NOT_APPLICABLE:
			pass
			
		# tag values are uncertain so we put it in a special queue to be dealt with later
		rules.MIGHT_TRUE, rules.MIGHT_FALSE:
			if !queue_query.has(tag_dep):
				queue_query.append(tag_dep)

#16. private methods


class BoolHolder:
	"""Act as a pointer / reference for boolean values"""
	var value: bool




# [< Comment date 2020 Apr 28; 16:24pm SAST >] TODO: consider instead of graph nodes, use scene tree and scenes
