class_name TagRegular
extends Tag
"""
Creation date: 2020 June 27
Author: EthanLR (Megaguy32)
Description: regular tag, to distinguish regular tags from operator tags

this code tries to follow PEP 8 coding style, recommended by Godot documentation (3.2)
"""

# 05. signals
# 06. enums
# 07. constants

# Section 08. exported variables
#export(String) var name
#export(Array, Resource) var dependers
#export(Array, Resource) var dependees
# - - - end section - - -


# 09. public variables
#main depender
# - - - end section - - -


# 10. private variables
# 11. onready variables
#
# 12. optional built-in virtual _init method
# 13. built-in virtual _ready method
# 14. remaining built-in virtual methods


# 15. public methods


# 16. private methods
