class_name TagOperatorNot
extends Tag
"""
Creation date: 2020 June 07
Author: EthanLR (Megaguy32)
Description: NOT version of tag, inverts main depender

this code tries to follow PEP 8 coding style, recommended by Godot documentation (3.2)
"""

# 05. signals
# 06. enums
# 07. constants

# Section 08. exported variables
#export(String) var name
#export(Array, Resource) var dependers
#export(Array, Resource) var dependees
# - - - end section - - -


# 09. public variables
#main depender
# - - - end section - - -


# 10. private variables
# 11. onready variables
#
# 12. optional built-in virtual _init method
# 13. built-in virtual _ready method
# 14. remaining built-in virtual methods


# 15. public methods
func operate():
	# Return true if all dependees are false
	var return_val := true
	for dependee in dependees:
		if dependee.get_tag_value() == true:
			return_val = false
			break
			
	set_tag_value(return_val)

# 16. private methods
