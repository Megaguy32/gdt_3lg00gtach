#01. tool

#02. class_name
class_name TagDepTreeTransitiveItem

#03. extends
extends Object

#04. # docstring
"""
This code tries to follow the PEP 8 coding style, recommended by Godot documentation
	https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html

Creation date: ***
Author: EthanLR (Megaguy32)
Description: ***


"""

#05. signals
#06. enums
#07. constants
#08. exported variables
#09. public variables
#10. private variables
#11. onready variables

#12. optional built-in virtual _init method
func _init(tag: Tag):
	self.tag = tag

#13. built-in virtual _ready method
#14. remaining built-in virtual methods
#15. public methods
#16. private methods


var tag: Tag # has reference to tag | corrensponding tag
var children: Array
var parent#: TagDepTreeTransitiveItem
var class_tag_dep_tree_trans = load("res://scripts/tag_classes/trans_tag_dep_tree_item.gd") # because cyclic dependency

	
func add_tag(tag) -> TagDepTreeTransitiveItem:
	var tree_item = class_tag_dep_tree_trans.new(tag)
	add_tree_item(tree_item)
	return tree_item
	
	
	
func add_tree_item(tree_item):#: TagDepTreeTransitiveItem):
	children.append(tree_item)
	tree_item.parent = self
	
