class_name PlayerCharacter
extends KinematicBody2D
"""
Creation date: 2020 Apr 09
Author: EthanLR (Megaguy32)
Description: code to make the main character object work

this code tries to follow PEP 8 coding style, recommended by Godot documentation (3.2)
"""

"""
tags:
	these are abstractions of states, they are not states, so no isIdle tag.
	unles idle becomes a tag in itself... im still ironing this out
	
	shadow tags are so that tag changes dont take immedeate effect (denoted by stag)
	I refresh shadow tags at the beginning of _process, so that the actions that take place are dependant on what state the character was in at the beginning of the frame.
	Shadow tags are to be read only. as if the tags cast a shadow.
	
	maybe make complex tags that depend on true/false values of various tags and do away with state variable. but for now state simplifies things in a way.
		
		
[< Comment date 2020 Apr 09 >]
	I'm removing shadow tags in favour of getters, setters & deferred calls
	because deferred calls on the idle frame, which is the time before 
		_process is called for all nodes.
		
		classes/class_scenetree.html?highlight=idle frame
		getting_started/scripting/gdscript/gdscript_basics.html#setters-getters
			
	As for tags, I'm thinking of enumerating them and placing them in an
	array, then just check if a tag is contained in the array.
	...
	did some research and nope, dictionary is functionally better
	in this case. as I'm able to make a check if dict contains variable
	tags in one fell swoop.
	
	and im going for the complex tags idea, where tags rely on
	other tags being true, getting rid of absolute states.
"""

# Section 05. signals:
# - - - end section - - -


# Section 06. enums
# - - - end section - - -


# Section 07. constants

const ATTACK_NODE: PackedScene = preload("res://characters/AttackArea.tscn")

# - - - end section - - -


# Section 08. exported variables

export (float) var walk_speed: float = 8
export (float) var jump_speed: float = -8
#export (float) var change_a_remainder: float = 0.5
#export (float) var change_v_initial: float = -30
export (float) var dampen := 1
export (Vector2) var gravity := Vector2(0, 0.5)

# - - - end section - - -


# Section 09. public variables

# For enemy fighting queue
		# TODO: Consider making modular, like a "Queue here" extention object
var enemy_fighter_head : Enemy
var enemy_fighter_tail : Enemy

var move_velocity: Vector2 = Vector2(0,0)
var move_collision: KinematicCollision2D = null
var input_vector: Vector2 = Vector2()

var tags: Tags = Tags.new({
	# simple tags
	"controlling": false,
	"gaurding": false,
	"attacking": false,
	"hurting": false,
	"moving": false,
	"hunkering": false,

	# complex tags
	"airbourning": false,
	"airbourning_phase_ascending": false,
	"airbourning_phase_peaking": false,
	"airbourning_phase_descending": false,
	"jumping": false,
	"moon_jumping": false,
	"moon_jumping_phase_initialing": false,
	"moon_jumping_phase_resulting": false,
	"regular_jumping": false,

	"crouching": false,
	"sliding": false,
	"normal_posturing": false,
	"idling": false,
	"stancing": false,
	"walking": false,

	"battling": false,
	"battling_phase_fighting": false,
	"battling_phase_recovering": false,

	# potential complex tags
#	"knockouting": false,
#	"running": false,
#	"dashing": false,
})

var tags_experimental: Tags = Tags.new({
	# simple tags
	"controlling": preload("res://custom_resources/tags_v2020_04_29/tag_controlling.tres"),
	"gaurding": preload("res://custom_resources/tags_v2020_04_29/tag_gaurding.tres"),
	"attacking": preload("res://custom_resources/tags_v2020_04_29/tag_attacking.tres"),
	"hurting": preload("res://custom_resources/tags_v2020_04_29/tag_hurting.tres"),
	"moving": preload("res://custom_resources/tags_v2020_04_29/tag_moving.tres"),
	"hunkering": preload("res://custom_resources/tags_v2020_04_29/tag_hunkering.tres"),

	# complex tags
	"airbourning": preload("res://custom_resources/tags_v2020_04_29/tag_airbourning.tres"),
	"airbourning_phase_ascending": preload("res://custom_resources/tags_v2020_04_29/tag_airbourning_phase_ascending.tres"),
	"airbourning_phase_peaking": preload("res://custom_resources/tags_v2020_04_29/tag_airbourning_phase_peaking.tres"),
	"airbourning_phase_descending": preload("res://custom_resources/tags_v2020_04_29/tag_airbourning_phase_descending.tres"),
	"jumping": preload("res://custom_resources/tags_v2020_04_29/tag_jumping.tres"),
	"moon_jumping": preload("res://custom_resources/tags_v2020_04_29/tag_moon_jumping.tres"),
	"moon_jumping_phase_initialing": preload("res://custom_resources/tags_v2020_04_29/tag_moon_jumping_phase_initialing.tres"),
	"moon_jumping_phase_resulting": preload("res://custom_resources/tags_v2020_04_29/tag_moon_jumping_phase_resulting.tres"),
	"regular_jumping": preload("res://custom_resources/tags_v2020_04_29/tag_regular_jumping.tres"),

	"crouching": preload("res://custom_resources/tags_v2020_04_29/tag_crouching.tres"),
	"sliding": preload("res://custom_resources/tags_v2020_04_29/tag_sliding.tres"),
	"normal_posturing": preload("res://custom_resources/tags_v2020_04_29/tag_normal_posturing.tres"),
	"idling": preload("res://custom_resources/tags_v2020_04_29/tag_idling.tres"),
	"stancing": preload("res://custom_resources/tags_v2020_04_29/tag_stancing.tres"),
	"walking": preload("res://custom_resources/tags_v2020_04_29/tag_walking.tres"),

	"battling": preload("res://custom_resources/tags_v2020_04_29/tag_battling.tres"),
	"battling_phase_fighting": preload("res://custom_resources/tags_v2020_04_29/tag_battling_phase_fighting.tres"),
	"battling_phase_recovering": preload("res://custom_resources/tags_v2020_04_29/tag_battling_phase_recovering.tres"),
	
	"not_apa": preload("res://custom_resources/tags_v2020_04_29/tag_not_apa.tres"),
	"not_apd": preload("res://custom_resources/tags_v2020_04_29/tag_not_apd.tres"),
	"not_app": preload("res://custom_resources/tags_v2020_04_29/tag_not_app.tres"),
	"not_bm": preload("res://custom_resources/tags_v2020_04_29/tag_not_bm.tres"),
	"not_f": preload("res://custom_resources/tags_v2020_04_29/tag_not_f.tres"),
	"not_ha": preload("res://custom_resources/tags_v2020_04_29/tag_not_ha.tres"),
	"not_m": preload("res://custom_resources/tags_v2020_04_29/tag_not_m.tres"),
	"not_mj": preload("res://custom_resources/tags_v2020_04_29/tag_not_mj.tres"),
	"not_mjpi": preload("res://custom_resources/tags_v2020_04_29/tag_not_mjpi.tres"),
	"not_mjpr": preload("res://custom_resources/tags_v2020_04_29/tag_not_mjpr.tres"),
	"not_r": preload("res://custom_resources/tags_v2020_04_29/tag_not_r.tres"),
	"not_rj": preload("res://custom_resources/tags_v2020_04_29/tag_not_rj.tres"),
	"or_apd": preload("res://custom_resources/tags_v2020_04_29/tag_or_apd.tres"),
	"or_fr": preload("res://custom_resources/tags_v2020_04_29/tag_or_fr.tres"),
	"or_gah": preload("res://custom_resources/tags_v2020_04_29/tag_or_gah.tres"),
	"or_mjpi_mjpr": preload("res://custom_resources/tags_v2020_04_29/tag_or_mjpi_mjpr.tres"),
	"or_mjrj": preload("res://custom_resources/tags_v2020_04_29/tag_or_mjrj.tres"),
	

	# potential complex tags
#	"knockouting": false,
#	"running": false,
#	"dashing": false,
})

# - - - end section - - -


# Section 10. private variables
# A.K.A Please don't touch these locally
# - - - end section - - -


# Section 11. onready variables

onready var tag_hash = tags.get_to_write().hash() # for function do_stuff_1 and printing tag changes in general
	# [< comment date 2020 Apr 18; 23:46pm >] TODO: when updating tags, update the hash too.

# - - - end section - - -


# Section 12. optional built-in virtual _init method
func _init():
	variableKeepPlayerCharacter.player_character = self

# - - - end section - - -


# Section 13. built-in virtual _ready method

func _ready():
	determine_input_vector()

# - - - end section - - -


# Section 14. remaining built-in virtual methods

func _process(delta):
	
	# [< Comment date 2020 Apr 18; 16:45pm >]
		# new proposed paradigm, Codorganize v0.0
		# - - -
		# 
		# 1. apply stuff ()
			# apply stuff based on tag info
			# apply auxillery data to visible properties?
		# 2. set new tags to be
		# 3. determing aux data (environment check, input check(unless input check in own events), communication check (unless handelled in own event), tag check (this includes determining complex tags))
		# 
	
	# figuring things out here
	# stuff I as a coder am able to do
		# set unstable tags
		# set aux data
		# set visual properties
			# position
			# velocity
			# animation
			# kinematic character physics
			
		# read stable tags
		# read unstable tags
		# read aux data
		# read environment
		# read input
		# read communication
		
		# [< 18:22pm >]
			# Codorganize v0.1
			# - - -
			# 
			# . set visual properties based on (a.k.a read) aux data, stable tags
			# . 
			# .
			# . set unstable tags (simple) based on aux data
			# . set unstable tags (complex) based on aux data, unstable tags
			# . 
			# . set aux data based on environment, input, communication, stable tags
			# . 
			# . 
	
	# [< Comment date 2020 Apr 19; 00:15am >] TODO: consider match statment without continue for mutualy exclusive goals
	# [< Comment date 2020 Apr 19; 00:58am >] TODO: consider making tag strings constant variables so I as a coder don't make spelling mistakes, that or modify the tags object to not accept spelling mistakes
	# [< Comment date 2020 Apr 19; 01:16am >] TODO: state / tag system is a lot like package management in linux, consider looking into pre made solutions and also dont forget looking into state machines.
		# take into account - setting true: dependencies, "NOT" dependents
		# take into account - setting false: dependents
	
	# read stable tags, set aux data, set visual properties?
	do_stuff_1()
	
	# goal: regular jump?
	# read stable tags, read aux data, set unstable tags
	if input_vector.y < 0 and tags.get_to_read()["airbourning"] == false:
		tags.get_to_write()["moving"] = true # dependency
		tags.get_to_write()["airbourning"] = true # dependency
		tags.get_to_write()["jumping"] = true # dependency
		tags.get_to_write()["regular_jumping"] = true # target
		# set move_velocity?
		
		move_velocity.y = -18
		
		
	# goal: apply gravity 
	# read stable tags, set aux data
	if tags.get_to_read()["airbourning"] == true:
		move_velocity += gravity
		
	# goal: walk
	# read stable tags, read aux data, set aux data
	if input_vector.x != 0:
		move_velocity.x = input_vector.x * walk_speed # [< Comment date 2020 Apr 10 >] TODO: consider move per second
		
		if tags.get_to_read()["airbourning"] == false:
			tags.get_to_write()["moving"] = true # dependency
			tags.get_to_write()["hunkering"] = false # dependency
			tags.get_to_write()["airbourning"] = false # dependency
			tags.get_to_write()["normal_posturing"] = true # dependency
			tags.get_to_write()["walking"] = true # target
			
	else:
			move_velocity.x = 0
			tags.get_to_write()["walking"] = false # target, has no dependents
			
	# goal: hunker
	# read aux data, set unstable tag
	if input_vector.y == 1:
		tags.get_to_write()["walking"] = false # "NOT" dependent
		tags.get_to_write()["stancing"] = false # "NOT" dependent
		tags.get_to_write()["idling"] = false # "NOT" dependent
		tags.get_to_write()["normal_posturing"] = false # "NOT" dependent
		tags.get_to_write()["hunkering"] = true # target
		
		
	# goal: apply simplified physics system
	# read stable tags, read aux data, set visual properties
	move_collision = move_and_collide(move_velocity)
	
	if move_collision != null:
		# repeatedly move along surfaces
		# [< comment date 2020 Apr 18; 22:38pm >] TODO: consider checking if the angle / normal change / delta is too strenuous (e.g. too steep) look into improving "check_if_slope" function
		while true:
			var the_normal := move_collision.get_normal()
			var move_velocity = move_collision.remainder.slide(the_normal) # get the new vect that shall move up the assumed slope
			if move_velocity == Vector2(0, 0): # stop if wall is hit (relatively speaking)
				break
			move_collision = move_and_collide(move_velocity)
			if move_collision == null: # stop if moved without problem
				break
		move_velocity = Vector2(0, 0)
	
	# [< Comment date 2020 Apr 18; 23:01pm >] TODO: make this work even though # repeatedly move along surfaces makes the vector (0, 0). consider introducing accurate and effective value
	move_velocity.x = move_toward(move_velocity.x, 0, dampen)
	# - - end goal - -
	
	# goal: attack
	# read stable tags, read aux data read environment / visual properties, sets unstable tags TODO: figure out what else this sets, might be something new like "set new instance" or something
	# [< Comment date 2020 Apr 18; 23:10pm >] TODO: Improve this, consider do on attacking initial signal
	if tags.get_to_read()["attacking"] == true:
		var attack_node: Node2D = ATTACK_NODE.instance()
		attack_node.position = position + Vector2(32, -24)
		get_tree().current_scene.add_child(attack_node)
		tags.get_to_write()["attacking"] = false
		
	# goal: set tag moving to false
	# read stable tags, read aux data set unstable tags
	# consider it an orphaned tag
	# [< Comment date 2020 Apr 19; 00:37am >] TODO: should actually read unstable tags, this should actually be grouped under the determine complex tags
	# no tags dependent on moving to be true are active, and move_velocity is 0
	if tags.get_to_read()["airbourning"] == false and tags.get_to_read()["sliding"] == false and tags.get_to_read()["walking"] == false and move_velocity == Vector2(0, 0):
		tags.get_to_write()["moving"] = false
		
	
	# goal: set tag jumping to false
	# read stable tags, read aux data set unstable tags
	# consider it an orphaned tag
	# [< Comment date 2020 Apr 19; 00:37am >] TODO: should actually read unstable tags, this should actually be grouped under the determine complex tags
	# no tags dependent on this tag to be true are active, and move_velocity is 0
	
	
	
	# read stable tags, read unstable tags?, set stable tags
	call_deferred("determine_complex_tags")
	
	# read input, read aux data, set aux data
	determine_input_vector()
	
	# [< Comment date 2020 Apr 10 >] TODO: look into unhandled input
	# godot-docs%20html%203.2/tutorials/inputs/inputevent.html
	
	# read input, set unstable tags
	if Input.is_action_pressed("ui_accept"):
		tags.get_to_write()["attacking"] = true
		
	# environment check
	# read env, set unstable tags
	# check if player is on ground or in air
	var is_collided := test_move(transform, Vector2(0, 1))
	if is_collided:
		# land
		# TODO: consider improving, shouldn't have to set move_velocity here!, consider making land a seperate goal
			# ok this has become more complicated than I anticipated, state change requires the dependents to be set to false, so its like dependencies in reverse. think about this
		move_velocity.y = 0
		tags.get_to_write()["regular_jumping"] = false # reverse dependency
		tags.get_to_write()["moon_jumping_phase_resulting"] = false # reverse dependency
		tags.get_to_write()["moon_jumping_phase_initialing"] = false # reverse dependency
		tags.get_to_write()["moon_jumping"] = false # reverse dependency
		tags.get_to_write()["jumping"] = false # reverse dependency
		tags.get_to_write()["airbourning_phase_ascending"] = false # reverse dependency
		tags.get_to_write()["airbourning_phase_peaking"] = false # reverse dependency
		tags.get_to_write()["airbourning_phase_descending"] = false # reverse dependency
		tags.get_to_write()["airbourning"] = false # target
#		print_debug("LAND")
		
	if !is_collided:
		tags.get_to_write()["moving"] = true # dependency # being airbourning implies moving, being grounded does not (but player might still be moving)
		tags.get_to_write()["airbourning"] = true # target
#		print_debug("NOT LAND!")
	

# - - - end section - - -


# Section 15. public methods

# [< Comment date 2020 Apr 10 >] TODO: rethink actions and passive. Think about cause of tags, and impact of tags. and goal based programming
# [< Comment date 2020 Apr 19; 00:06am >] TODO: consider replcing these passive functions with on tag changed signal. or perhaps not. because tags may 'be' for a duration of time
# Passive Functions	
func passive_airbourne(): pass
func passive_grounded(): pass
func passive_move(): pass
func passive_attack(): pass
func passive_idle(): pass
func passive_walking(): pass
func passive_jumping(): pass
func passive_regular_jump(): pass
func passive_crouching(): pass
func passive_sliding(): pass


func do_stuff_1():
	# this prints tag changes in console
	# and this calls passive functions
	
	var is_tag_handled := false
	var debug_flag_should_report_passive_handle := true 
	var report_passive_handle := "I, a PlayerCharacter named " + name + ", am" + "\n\t"
	
	match tags.get_to_read():
		# complex tags excluded
		# [< Comment date 2020 Apr 19; 00:52am >] TODO: put all tags in here or just print the whole dictionary
		{"airbourning": true, ..}:
			is_tag_handled = true
			report_passive_handle += "airbourning, "
			passive_airbourne()
			continue
			
		{"airbourning": false, ..}:
			is_tag_handled = true
			report_passive_handle += "not airbourning, "
			passive_grounded()
			continue
			
		{"moving": true, ..}:
			is_tag_handled = true
			report_passive_handle += "moving, "
			passive_move()
			continue
			
#		{"moon_jumping": true, ..}:
#			is_tag_handled = true
#			report_passive_handle += "moon jumping, "
#			passive_moonjump(delta)
#			continue
			
		{"regular_jumping": true, ..}:
			is_tag_handled = true
			report_passive_handle += "regular jumping, "
			passive_regular_jump()
			continue
			
		{"attacking": true, ..}:
			is_tag_handled = true
			report_passive_handle += "attacking, "
			passive_attack()
			continue
			
		# complex tags included
		{"idling": true, ..}:
			is_tag_handled = true
			report_passive_handle += "idle, "
			passive_idle()
			continue
			
		{"walking": true, ..}:
			is_tag_handled = true
			report_passive_handle += "walking, "
			passive_walking()
			continue
			
		{"jumping": true, ..}:
			is_tag_handled = true
			report_passive_handle += "jumping, "
			passive_jumping()
			continue
			
		{"hunkering": true, ..}:
			is_tag_handled = true
			report_passive_handle += "hunkering, "
			continue
				
	if debug_flag_should_report_passive_handle:
		if tag_hash != tags.get_to_write().hash(): #tags changed
			if is_tag_handled == false:
				report_passive_handle += "in an unhandled state (tag combo)" +"\n" + "tags: " + str(tags.get_to_read())
			print_debug(report_passive_handle)
			tag_hash = tags.get_to_write().hash()


# [< Comment date 2020 Apr 10 >] TODO: consider implemementing input using input map
# Get input from keyboard and put it in vector form
func determine_input_vector(): 
	input_vector = Vector2(0,0)
	
	if Input.is_action_pressed("ui_right"):
		input_vector.x += 1
	if Input.is_action_pressed("ui_left"):
		input_vector.x -= 1
	if Input.is_action_pressed("ui_up"):
		input_vector.y -= 1
	if Input.is_action_pressed("ui_down"):
		input_vector.y += 1
		
	input_vector = input_vector.normalized()

"""
[< Comment date 2020 Apr 10 >] TODO:
		There might be a problem here. since I'm using deferred calls
		the tags only get updated at the end,
		but the entire dictionary changes in the deferred function,
		so some tags may be overwritten.
		Consider testing.
		...
		yup, I was right. it is a problem. eish.
		Might actually be a bigger problem that affects others too.
		
		because if you setget a dictionary
		e.g.
			dict = {0: false} setget setter
			
			setter():
				pass
				
		and do elsewhere: myobj.dict[0] = true 
		
		then dict[0] is true anyway even though the setter should prevent that.
		TODO:
			tell the godot community about that
		...
		Github issue, https://github.com/godotengine/godot/issues/37737
		
		Apparently its not a bug? that confuses me. but ok its workaround time.
		and the simplest solution is to bring the shadow tag back, but
		now its a duplicate dicionary, and its written to not read from,
		still making use of a deferred call.
		...
		
		Ok ok ok, it took a few brain farts but I got the desired effect with 
		setget. by returning a duplicate  
		...
		too much fart. check in later.
"""
func determine_complex_tags():
	
	# [< Comment date 2020 Apr 10 >] TODO: consider emiting state (tag combo / tags hash) changed signal
	# [< Comment date 2020 Apr 10 >] TODO: consider enumerating tags for code completion sake
#	var th = tags.get_to_write().hash()
	# instead of using tag get to read, use get to write for reading and writing, because complex tags depend on simple tags, which may be outdated, this method should be called just before Tags applies its unapplied tags
	var is_tag_handled := false

	var debug_flag_should_report_complex_tags := false 
	var complex_tag_report := ""
	
	tags.get_to_write()["idling"] = false
	
	# [< Comment date 2020 Apr 18; 23:58pm >] TODO: consider instead of enforcing tags true and false, rather just check if the setup is valid (check if true, instead of set to true) and if invalid throw error (print debug, assert false)
	
	match tags.get_to_read():
		{"airbourning": false, "moving": false, "jumping": false, "attacking": false, ..}:
			is_tag_handled = true
			tags.get_to_write()["idling"] = true
#			complex_tag_report += ("I, a PlayerCharacter named " + name + ", am idle\n")
			continue
				
				
#		_:
#			if is_tag_handled == false: pass
#				print_debug("I, a PlayerCharacter named " + name + ", have not determined any complex tags")
#				print_debug("here's the tags:", str(tags.get_to_read()))
	
	if debug_flag_should_report_complex_tags:
		if tag_hash != tags.get_to_write().hash(): #tags changed
			if is_tag_handled == false:
				complex_tag_report = "I, a PlayerCharacter named " + name + ", have not determined any complex tags"
			print_debug(complex_tag_report)
		tag_hash = tags.get_to_write().hash()
	


func check_if_slope(the_normal): # [< 2020 Apr 18; 22:44pm >] TODO: consider more accurate rename
	#checks if normal of collision is facing upwards (checking if its a gradual slope (true), or steep slope / wall (false))
	var vector_1 = Vector2( 0.9,-1) #0.9 is for a little bit of leniency
	var vector_2 = Vector2(-0.9,-1)
	
	var report = (vector_1.dot(the_normal) >= 0) and (vector_2.dot(the_normal) >= 0)
	return report


func receiveKnockback(v):
	print("EINA")
	move_and_collide(Vector2(-50,0))


func hurt(hit_value: float, knockback: Vector2):
		print_debug("I, Player Character " + name + " am gehit with a value of: " + str(hit_value))
		tags.get_to_write()["hurting"] = true

# - - - end section - - -


# Section 16. private methods
# A.K.A Please don't touch these locally
# - - - end section - - -





func _on_Button_pressed():
	print("ge press")
	tags.get_to_write()["attacking"] = true
