#01. tool

#02. class_name
class_name TagsExperimental

#03. extends
extends Object

#04. # docstring
"""
This code tries to follow the PEP 8 coding style, recommended by Godot documentation
	https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/gdscript_styleguide.html

Creation date: 2020 June 10
Author: EthanLR (Megaguy32)
Description: Represent tags with a collection of tag resources.
	allows for transitive dependencies, and being printed so it may be displayed.

TODO: merge this with tags_stable.gd
"""

#05. signals
#06. enums
#07. constants
const DEBUG_IS_PRINT := true #print out debbuging information if true
#08. exported variables
#09. public variables
var tags : Array # Array of Tag

#10. private variables
#11. onready variables

#12. optional built-in virtual _init method
func _init(var tags_arr: Array):
#	print("tags init")
	tags = tags_arr
#	print("tags + yobe")
#	print(tags_dict)
	
#	add_associated_dependers()
	
	for tag in tags:
		create_tag_dep_tree_transitive(tag)

#13. built-in virtual _ready method
#14. remaining built-in virtual methods
func _to_string():
	var return_val = "" #"tags is to stringing, HAZZAH!"
	for tag in tags:
		return_val += "\n" + str(tag)
	return return_val

#15. public methods
# What is required here is to create a tree of transitive dependencies
# one for dependers of the target tag,
# one for dependees of the target tag.
func create_tag_dep_tree_transitive(tag_target: Tag):
	
#	print("\nCreating dependees tree for tag, " + tag_target.name + "\n - - -")
	tag_target.dependees_transitive = TagDepTreeTransitiveItem.new(tag_target)
	fill_tree_dependees(tag_target.dependees_transitive)
#	print("Created dependees tree.")
	
#	print("\nCreating dependers tree for tag, " + tag_target.name + "\n - - -")
	tag_target.dependers_transitive = TagDepTreeTransitiveItem.new(tag_target)
	fill_tree_dependers(tag_target.dependers_transitive)
#	print("Created dependers tree.")


func fill_tree_dependees(tree_item_root: TagDepTreeTransitiveItem):
#	print("fill tree dependees")
	var tags_handled := [tree_item_root.tag] # array / list of tags that we're done with
	var tree_item_queue := [tree_item_root]
#	print("tree dependees: " + str(tree_item_root.tag.name))
	# traverse through queue
	for tree_item_current in tree_item_queue:
#		print("\n")
#		print("tree item current: " + str(tree_item_current))
#		print("tree item current tag: " + str(tree_item_current.tag))
#		print("tree item current tag name: " + str(tree_item_current.tag.name))
#		print("tree item current tag dependees: " + str(tree_item_current.tag.dependees))
#		print("\n")

		# add dependees of current tag to the queue
		for tag_dep in tree_item_current.tag.dependees:
			# skip this tag if it's already handled
			if tags_handled.has(tag_dep):
				continue
			
			# add tag to transitive dependency tree
			var tree_item_new = tree_item_current.add_tag(tag_dep)
			tree_item_queue.append(tree_item_new)
			
			# add tag to handled list
			tags_handled.append(tag_dep)
			
#			print("added tag, " + str(tag_dep) + ", to tree")


func fill_tree_dependers(tree_item_root: TagDepTreeTransitiveItem):
#	print("fill tree dependers")
	var tags_handled := [tree_item_root.tag] # array / list of tags that we're done with
	var tree_item_queue := [tree_item_root]
	
	
	# traverse through queue
	for tree_item_current in tree_item_queue:
#		print("\n")
#		print("tree item current: " + str(tree_item_current))
#		print("tree item current tag: " + str(tree_item_current.tag))
#		print("tree item current tag name: " + str(tree_item_current.tag.name))
#		print("tree item current tag dependers: " + str(tree_item_current.tag.dependers))
#		print("\n")

		# add dependees of current tag to the queue
		for tag_dep in tree_item_current.tag.dependers:
			# skip this tag if it's already handled
			if tags_handled.has(tag_dep):
				continue
			
			# add tag to transitive dependency tree
			var tree_item_new = tree_item_current.add_tag(tag_dep)
			tree_item_queue.append(tree_item_new)
			
			# add tag to handled list
			tags_handled.append(tag_dep)
			
#			print("added tag," + str(tag_dep) + ", to tree")


func get_required_tag_dep_values(tag_target: Tag, value_to_be: bool) -> Dictionary:
#	print("get_required_tag_dep_values()")
	if DEBUG_IS_PRINT:
		print("\nGet required tag dep values of: " + str(tag_target.name) + ", set to: " + str(value_to_be) + "")
	
	#go through tree items carrying values applicable, value_to_be, var_inverted_by_not, var_inverted_by_vtb.
	var applicable: BoolHolder # is the tag required to be a certain value
	var vtb: BoolHolder # Value to be
	var var_inv_not: BoolHolder # Variable inverted by NOT
	var var_inv_vtb: BoolHolder # Variable inverted by Value to be
	var root
	
	# dependees
	applicable = BoolHolder.new() # necessary line to declare the reference here, otherwise its null and doesn't count as a pointer. Results in null pointer exception later down the road
	vtb = BoolHolder.new() # necessary line, same as applicable
	# NOT inverts value to be
	var_inv_not = vtb
	# value_to_be inverts applicable
	var_inv_vtb = applicable
	root = tag_target.dependees_transitive
	
	var return_value = get_tag_dep_values(tag_target, value_to_be, applicable, vtb, var_inv_not, var_inv_vtb, root)
	return return_value


func get_impacted_tag_dep_values(tag_target: Tag, value_to_be: bool) -> Dictionary:
	#go through tree items carrying values applicable, value_to_be, var_inverted_by_not, var_inverted_by_vtb.
	var applicable: BoolHolder # is the tag required to be a certain value
	var vtb: BoolHolder # Value to be
	var var_inv_not: BoolHolder # Variable inverted by NOT
	var var_inv_vtb: BoolHolder # Variable inverted by Value to be
	var root
	
	# dependers
	applicable = BoolHolder.new() # necessary line to declare the reference here, otherwise its null and doesn't count as a pointer. Results in null pointer exception later down the road
	vtb = BoolHolder.new() # necessary line, same as applicable
	# NOT inverts applicable,
	var_inv_not = applicable
	# value_to_be inverts applicable
	var_inv_vtb = applicable
	root = tag_target.dependers_transitive
	
	var return_value = get_tag_dep_values(tag_target, value_to_be, applicable, vtb, var_inv_not, var_inv_vtb, root)
	return return_value

#Function called by get_required_tag_dep_values() or get_impacted_tag_dep_values()
func get_tag_dep_values(tag_target: Tag, value_to_be: bool, applicable: BoolHolder, vtb: BoolHolder, var_inv_not: BoolHolder, var_inv_vtb: BoolHolder, root) -> Dictionary:
#	print("var_tag_dep_values()")
	
	var tag_list_resources := [] # Array / list of tag resources
	var tag_list_values := [] # Array of bools, representing the required values
	
	#go through tree items carrying values applicable, value_to_be, var_inverted_by_not, var_inverted_by_vtb.
	applicable.value = true
	vtb.value = value_to_be
	
	
	
	get_tag_dep_values_recursive(applicable, vtb, var_inv_not, var_inv_vtb, tag_list_resources, tag_list_values, root)
	
	var return_value = {
		"resources": tag_list_resources,
		"values": tag_list_values,
	}
	if DEBUG_IS_PRINT:
		print("The tags to be are: ")
		print_tags_to_be(return_value)
	
	return return_value

# Recursive method of finding required / impacted tags
var class_tag_op_not = load("res://scripts/tag_classes/tag_operator_not.gd") # because cyclic dependency
func get_tag_dep_values_recursive (applicable: BoolHolder, vtb: BoolHolder, var_inv_not: BoolHolder, var_inv_vtb: BoolHolder, tag_list_resources: Array, tag_list_values: Array, tree_item: TagDepTreeTransitiveItem):
	if tree_item.tag is class_tag_op_not:
		var_inv_not.value = !var_inv_not.value

	if vtb.value:
		var_inv_vtb.value = !var_inv_vtb.value

	if applicable:
		# add tag to tags_to_be
		tag_list_resources.append(tree_item.tag)
		tag_list_values.append(vtb.value)

	for tree_item_dep in tree_item.children:
		get_tag_dep_values_recursive(applicable, vtb, var_inv_not, var_inv_vtb, tag_list_resources, tag_list_values, tree_item_dep)


func get_tags():
	return tags


func set_tags_to_be(tags_to_be: Dictionary) -> void:
	for i in range(tags_to_be["resources"].size()):
		tags_to_be["resources"][i].set_tag_value( tags_to_be["values"][i] )
		
func set_tag(tag_target: Tag, value: bool):
	"""set tag value and handle tag dependencies"""
	var tags_to_be
	tags_to_be = get_required_tag_dep_values(tag_target, value)
	set_tags_to_be(tags_to_be)
	tags_to_be = get_impacted_tag_dep_values(tag_target, value)
	set_tags_to_be(tags_to_be)
	
	# [< Comment date 2020 Jun 12 01h54 >] might be redundant, because set_tags_to_be may already set tag to true
#	tag_target.set_tag_value( value ) # target 
	# [< Comment date 2020 Jun 19 21h19 >] yes. target tag is included in tags to be


func print_tags_to_be(tags_to_be) -> void:
	var the_text = ""
	the_text += "Tags to be:"
	for i in range(tags_to_be["resources"].size()):
		the_text += "\n\t" + str(tags_to_be["resources"][i].name) + " to be " + str(tags_to_be["values"][i])
	the_text += "\n"
	print(the_text)


#16. private methods


class BoolHolder:
	"""Act as a pointer / reference for boolean values"""
	var value: bool




# [< Comment date 2020 Apr 28; 16:24pm SAST >] TODO: consider instead of graph nodes, use scene tree and scenes
