##Update to version 0.2.0

**Download at**	[Game Jolt page](https://gamejolt.com/games/3lg00gtach/405225)

**Source code at** [Gitlab page](https://gitlab.com/Megaguy32/gdt_3lg00gtach/)

![](tag-flowchart)

Salutations fellow enthusiasts (◕‿◕)つ

Because the game is in a workable state
and I hit a milestone with my "Concurrent State machine" / "Tag based state system"
and movement is smooth (not complete but smooth)

I have updated my game. HUZZAH!

3 Major things I worked on in this game recently (past few months) is

- Player Character States (Currently working)
- Enemy Fighting Logic (Still WIP)
- Moon Jump mechanic (Still WIP)

I've been working this year on my project a lot and
Its been a long unapologetic grind, and I'm not even halfway done with what
I set out to do, I actually got sidetracked with the Tag State system, since
that wasn't actually on my original to-do list. but I hope I'm able to move
forward with greater velocity from here.

but anyway, the Moon jumping mechanic should be my next big focus, along with
Enemy fighting logic. The walking down slopes may slot in arbitrarily somewhere
along the line.
<br/>

New:

- Added new tag based state system ([Explanatory Video](https://youtu.be/we6dBRmIjpg))
- Added a bunch of display elements for Debugging purposes.
- Added extra documentation (in git repo)

Improved:

- Movement code

Broken:

- attacking code is added, but also broken
- Enemy fighting logic is added, and is also broken
- Levels besides main menu and first level
- Jumping
<br/>

###Comment on Focus points:
####Tilemaps & Collision:
Seemingly the simplest of the bunch, just followed the tutorials and its
working out nicely
<br/>
####Pixel perfect collision:
I'm opting not to focus on this in favour of using Godot's built-in
collision system
<br/>
####Slopes:
Currently able to walk smoothly up slopes
Moving Down slopes is coming soon.
In the Git repo you're able to find the relevant code under plc_movement.gd
<br/>
####A cool jump mechanic I’ve been working on:
Currently not implemented yet. Still doing research and figuring out a
system that works.
<br/>
####My own interpretation of a state machine:
This is a new focus point I added
Currently working (I hope) and that's a major milestone and the reason
for my update.
